#!/usr/bin/make -f
# debian/rules for Free Pascal from Debian (modified for Maemo)

DEB_PACKAGE_NAME = fpc
DEB_DH_BUILDDEB_ARGS := -- -Z bzip2
export DH_ALWAYS_EXCLUDE := COPYING:LICENSE
export LANG:=C

# Documentation type to use pdf/html
ifeq (${DEBDOCTYPE},)
DOCTYPE=html
# HTML Converter l2h/4ht/hevea or html for (default)
CONVERTER=hevea
else
DOCTYPE=${DEBDOCTYPE}
CONVERTER=${DOCTYPE}
endif
# Undefine FPC if it was set
unexport FPC

# Detect name of new compiler, take care that debian uses amd64 instead of x86_64
CPU_SOURCE := $(subst amd64,x86_64,$(shell dpkg-architecture -qDEB_BUILD_ARCH))
CPU_TARGET := $(subst amd64,x86_64,$(shell dpkg-architecture -qDEB_HOST_ARCH))

# Fixup armel, too
CPU_SOURCE := $(subst armel,arm,$(CPU_SOURCE))
CPU_TARGET := $(subst armel,arm,$(CPU_TARGET))

ifeq ($(CPU_TARGET),m68k)
PPSUF=68k
endif
ifeq ($(CPU_TARGET),i386)
PPSUF=386
endif
ifeq ($(CPU_TARGET),x86_64)
PPSUF=x64
endif
ifeq ($(CPU_TARGET),powerpc)
PPSUF=ppc
endif
ifeq ($(CPU_TARGET),alpha)
PPSUF=axp
endif
ifeq ($(CPU_TARGET),arm)
PPSUF=arm
endif
ifeq ($(CPU_TARGET),sparc)
PPSUF=sparc
endif

ifneq ($(CPU_SOURCE),$(CPU_TARGET))
PPPRE=ppcross
else
PPPRE=ppc
endif

PPNEW=$(PPPRE)$(PPSUF)

FPCTARGET=$(CPU_TARGET)-linux
# Get version information from changelog file
DEB_VERSION:=$(shell dpkg-parsechangelog | egrep '^Version:' | cut -f 2 -d ' ')
DEB_UPSTREAM_VERSION:=$(shell echo $(DEB_VERSION) | cut -f 1 -d -)
DEB_UPSTREAM_MAIN_VERSION:=$(shell echo ${DEB_UPSTREAM_VERSION} | sed -e 's/^\([0-9\.]*\).*/\1/')
DEB_BUILD=$(lastword $(subst -, ,${DEB_VERSION}))
ifndef PACKAGESUFFIX
export PACKAGESUFFIX=-${DEB_UPSTREAM_MAIN_VERSION}
endif
# Get directories
CURDIR:=$(shell pwd)
BUILD_DIR=$(CURDIR)/debian/build
INSTALL_DIR=$(CURDIR)/debian/opt/fpc

# Get utils
ifndef MKDIR
MKDIR=mkdir -p
endif
ifndef CP
CP=cp -Rfpl
endif
ifndef MV
MV=mv
endif
RM:=rm -rf
# Get fpcmake from path if none is specified.
ifndef FPCMAKE
FPCMAKE=fpcmake
endif
# Undefine FPCDIR if it was set
unexport FPCDIR
NEWPP=$(CURDIR)/compiler/$(PPNEW)
NEWFPDOC=$(CURDIR)/utils/fpdoc/fpdoc
#If not cross compiling, then use new generated fpcmake utility.
ifeq (${PPPRE},)
NEWFPCMAKE=$(CURDIR)/utils/fpcm/fpcmake
else
NEWFPCMAKE=${FPCMAKE}
endif
# Set default compilation options
BUILDOPTS=PP=$(NEWPP)
ifdef CROSSOPT
BUILDOPTS+= CROSSOPT=${CROSSOPT}
endif

export GDBLIBDIR=/usr/lib

#export DH_VERBOSE=1

###################
# Clean
#

clean: configure clean-patched
clean-patched:
	@echo "--- Cleaning"
	dh_testdir
	dh_testroot
	dh_prep

###################
# Debian files
#

debian-files: debian-files-stamp
debian-files-stamp:
	@echo "--- Creating/fixing *.install files"
	touch debian-files-stamp

###################
# Arch packages
#

configure: configure-stamp
configure-stamp:
	touch configure-stamp

build-arch: debian-files configure build-arch-stamp
build-arch-stamp:
	@echo "--- Building"
	dh_testdir
	$(MAKE) all PP=$(CURDIR)/debian/ppcrel_$(PPSUF)

	touch build-arch-stamp

install-arch: build-arch install-arch-stamp
install-arch-stamp:
	@echo "--- Installing"
	dh_testdir
	dh_testroot
	dh_installdirs

	#regular make install using just compiled compiled
	time make install PP=$(CURDIR)/compiler/ppc3 INSTALL_PREFIX=${INSTALL_DIR}
	
	#then put them to the correct subfolders

	#Note. e.g. $(FPCTARGET)= x86_64-linux

	#compiler -package
	${MKDIR} $(CURDIR)/debian/fp-compiler-2.4.4/usr/bin
	${MKDIR} $(CURDIR)/debian/fp-compiler-2.4.4/etc

	${MKDIR} $(CURDIR)/debian/fp-compiler-2.4.4/opt/fpc/bin
	${MKDIR} $(CURDIR)/debian/fp-compiler-2.4.4/opt/fpc/lib/fpc/2.4.4/msg/

	${MV} ${INSTALL_DIR}/bin/fpc $(CURDIR)/debian/fp-compiler-2.4.4/opt/fpc/bin
	${MV} ${INSTALL_DIR}/lib/fpc/2.4.4/msg/* $(CURDIR)/debian/fp-compiler-2.4.4/opt/fpc/lib/fpc/2.4.4/msg/
	${MV} ${INSTALL_DIR}/lib/fpc/2.4.4/samplecfg $(CURDIR)/debian/fp-compiler-2.4.4/opt/fpc/lib/fpc/2.4.4
	${MV} ${INSTALL_DIR}/lib/fpc/2.4.4/ppc$(PPSUF) $(CURDIR)/debian/fp-compiler-2.4.4/opt/fpc/lib/fpc/2.4.4/

		#generate fpc.cfg
	$(CURDIR)/debian/samplecfg /opt/fpc $(CURDIR)/debian/etc $(FPCTARGET)
	${MV} $(CURDIR)/debian/etc/fpc.cfg $(CURDIR)/debian/fp-compiler-2.4.4/etc


	#utils -package
	${MKDIR} $(CURDIR)/debian/fp-utils-2.4.4/opt/fpc/bin
	${MKDIR} $(CURDIR)/debian/fp-utils-2.4.4/opt/fpc/lib/fpc/lexyacc

	${MV} ${INSTALL_DIR}/bin/* $(CURDIR)/debian/fp-utils-2.4.4/opt/fpc/bin
	${MV} ${INSTALL_DIR}/lib/fpc/lexyacc/* $(CURDIR)/debian/fp-utils-2.4.4/opt/fpc/lib/fpc/lexyacc


	#units-rtl -package 
	${MKDIR} $(CURDIR)/debian/fp-units-rtl-2.4.4/opt/fpc/lib/fpc/2.4.4/units/$(FPCTARGET)/

	${MV} ${INSTALL_DIR}/lib/fpc/2.4.4/units/$(FPCTARGET)/rtl $(CURDIR)/debian/fp-units-rtl-2.4.4/opt/fpc/lib/fpc/2.4.4/units/$(FPCTARGET)/
	
	#units -package (this contains all units -packages (e.g. debian/ubuntu splits them several packages)
	${MKDIR} $(CURDIR)/debian/fp-units-2.4.4/opt/fpc/lib/fpc/2.4.4/
	${MV} ${INSTALL_DIR}/lib/fpc/2.4.4/units/ $(CURDIR)/debian/fp-units-2.4.4/opt/fpc/lib/fpc/2.4.4/


	#link to the path
	ln -s /opt/fpc/lib/fpc/2.4.4/ppc$(PPSUF) $(CURDIR)/debian/fp-compiler-2.4.4/usr/bin/fpc 

	touch install-arch-stamp

arrange-arch: install-arch arrange-arch-stamp install-man
arrange-arch-stamp:
	dh_testdir
	dh_testroot
	touch arrange-arch-stamp

build-indep:
install-indep:
	touch install-indep-stamp

install-man: install-man-stamp
install-man-stamp:
	touch install-man-stamp

###################
# Documentation
#

build-doc: debian-files build-doc-stamp
build-doc-stamp:
	touch build-doc-stamp

install-doc: build-doc install-doc-stamp
install-doc-stamp:
	@echo "--- Installing Documentation"
	touch install-doc-stamp

###################
# Source
#

install-source: install-source-stamp
install-source-stamp: 
	@echo "--- Cleaning the tree and copying the source code"
	dh_testdir
	dh_testroot
	touch install-source-stamp

###################
# Generic
#

build: build-arch build-indep
install: install-arch install-indep
binary: binary-arch binary-indep


###################
# Deb building
#

binary-indep: clean-patched build-doc install-doc install-source debian-files
	@echo "--- Building: arch-indep packages"
	dh_testdir
	dh_testroot
	dh_installdocs -i -X.in
	dh_installchangelogs -i

	dh_compress -i -X.pdf
	dh_fixperms -i
	dh_installdebconf -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i $(DEB_DH_BUILDDEB_ARGS)

binary-arch: arrange-arch
	@echo "--- Building: arch packages"
	dh_testdir
	dh_testroot
	dh_link
	dh_installdocs -a -X.in
	dh_installchangelogs -a
	dh_strip -s
	dh_compress -a
	dh_fixperms -a
	dh_installdebconf -a
	dh_installdeb -a
	dh_shlibdeps -s
	dh_gencontrol -s
	dh_md5sums -s
	dh_builddeb -s $(DEB_DH_BUILDDEB_ARGS)

.PHONY: build clean binary binary-arch \
	binary-indep debian-files build-arch \
	install install-indep install-arch \
	configure

get-orig-source:
	-uscan --upstream-version=0 --rename
